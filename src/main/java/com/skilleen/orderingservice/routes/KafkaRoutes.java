package com.skilleen.orderingservice.routes;

import com.skilleen.orderingservice.dto.Order;
import com.skilleen.orderingservice.dto.Orders;
import com.skilleen.orderingservice.dto.ShippingOrder;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.SagaPropagation;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class KafkaRoutes extends RouteBuilder {

    @Override
    public void configure() {
        from("direct:publish")
                .setBody(constant("Message from Scott"))
                .log("Sending Message!")
                .to("kafka:scotts-topic?brokers=172.30.74.234:9092");

        from("direct:create-shipping-request")
                .id("order-route")
                .saga()
                .propagation(SagaPropagation.SUPPORTS)
                .bean(this,"transformMessage")
                .marshal().json(JsonLibrary.Jackson)
                .log("Order confirmed! Sending to Shipping service..")
                .to("kafka:order-request?brokers=172.30.74.234:9092")
                .id("order-publish")
                .unmarshal().json(JsonLibrary.Jackson)
                .log("DONE")
                .end();

        from("direct:create-shipping-requests")
                .id("order-routes")
                .split().method(this,"splitOrders")
                .marshal().json(JsonLibrary.Jackson)
                .log("Order confirmed! Sending the following order to Shipping service..")
                .log("${body}")
                .to("kafka:order-request?brokers=172.30.74.234:9092")
                .id("order-publishs")
                .unmarshal().json(JsonLibrary.Jackson)
                .log("DONE")
                .end();

    }

    public void transformMessage(Exchange exchange){
        Message in = exchange.getIn();
        Order order = in.getBody(Order.class);
        ShippingOrder shippingOrder = new ShippingOrder(order.getName(), order.getPrice());
        in.setBody(shippingOrder);
    }

    public List<ShippingOrder> splitOrders(Exchange exchange){
        Message in = exchange.getIn();
        List<Order> orders = in.getBody(Orders.class).getOrders();
        return orders.stream().map(o -> new ShippingOrder(o.getName(), o.getPrice())).collect(Collectors.toList());
    }
}
